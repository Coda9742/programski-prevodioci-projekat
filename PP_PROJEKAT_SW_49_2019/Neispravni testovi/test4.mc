// Neispravni pozivi funkcije 


int[] funkcija(int a, int[] b, unsigned c, unsigned[] d)
{

	return b;
}

int main() {

	int[] niz;
	int x;
	int a;
	int b;
	unsigned c;
	unsigned d;
	
	int[] nizparam;
	unsigned[] nizparam2;
	
	niz = funkcija(); // bez parametara
	niz = funkcija(x,x,x); // los broj parametara
	niz = funkcija(x,x,x,x); // dobar broj parametara ali pogresni tipovi
	

	niz = funkcija(a,b,c,d); // Dobar broj i dobri tipovi, ali nema nizova gde treba da budu
	
	

	niz = funkcija(a, nizparam2, c, nizparam); // int[] i unsigned[] su zmanili mesta

	niz << funkcija(a, nizparam, c, nizparam2);  // Ispravan poziv
 	
}

