%{
  #include <stdio.h>
  #include <stdlib.h>
  #include "defs.h"
  #include "symtab.h"
  #include "codegen.h"

  int yyparse(void);
  int yylex(void);
  int yyerror(char *s);
  void warning(char *s);

  extern int yylineno;
  int out_lin = 0;
  char char_buffer[CHAR_BUFFER_LENGTH];
  int error_count = 0;
  int warning_count = 0;
  int var_num = 0;
  int fun_idx = -1;
  int fcall_idx = -1;
  int lab_num = -1;
  FILE *output;

  int* parameter_map[128];

  int parameter_array_map[128][10];
  int array_param_counter = 0;

  int arg_counter = 0;
%}

%union {
  int i;
  char *s;
}

%token <i> _TYPE
%token _IF
%token _ELSE
%token _RETURN
%token _FOR
%token _BREAK
%token _CONTINUE
%token <s> _ID
%token <s> _INT_NUMBER
%token <s> _UINT_NUMBER
%token _LPAREN
%token _PUSH
%token _AMPERSAND
%token _RPAREN
%token _COMMA
%token _LBRACKET
%token _RBRACKET
%token _LSQBRACKET
%token _RSQBRACKET
%token _ASSIGN
%token _INC
%token _DEC
%token _SEMICOLON
%token <i> _AROP
%token <i> _RELOP

%type <i> num_exp exp literal function_call rel_exp argument

%nonassoc ONLY_IF
%nonassoc ONLY_FOR
%nonassoc _ELSE



%%

program
  : global_list function_list
      {  
        if(lookup_symbol("main", FUN) == NO_INDEX)
          err("undefined reference to 'main'");
       }
  ;

global_list
  : /* empty */
  | global_list global_var
  ;

global_var
 
	: _TYPE _ID _SEMICOLON
	 
	{
	 
		int idx = lookup_symbol($2, VAR);
		 
		if (idx != NO_INDEX) {
		 
		err("redefinition of '%s'", $2);
	 
	}
	 
	else {
	 
		insert_symbol($2, VAR, $1, NO_ATR, NO_ATR);
		 
		code("\n%s:\n\t\tWORD\t1", $2);
	 
	}
	 
	}
	| _TYPE _LSQBRACKET _RSQBRACKET _ID _SEMICOLON
	
 	{
	 
		int idx = lookup_symbol($4, VAR);
		 
		if (idx != NO_INDEX) {
		 
		err("redefinition of '%s'", $4);
	 
	}
	 
	else {
	 
		insert_symbol($4, VAR, $1, NO_ATR, 1);
		 
		code("\n%s:\n\t\tWORD\t1", $4);
	 
	}
	 
	}
;

function_list
  : function
  | function_list function
  ;

function
  : _TYPE _ID
 
	{
	 
		fun_idx = lookup_symbol($2, FUN);
	 
		if(fun_idx == NO_INDEX){
	 
		int* param_types = (int*) malloc(sizeof(int)*128);
	 
		fun_idx = insert_symbol($2, FUN, $1, NO_ATR, NO_ATR);
	 
		parameter_map[fun_idx] = param_types;
	 
		}
	 
	else
	 
		err("redefinition of function '%s'", $2);

		code("\n%s:", $2);
		code("\n\t\tPUSH\t%%14");
		code("\n\t\tMOV \t%%15,%%14");
	 
	}
	 
	_LPAREN parameter_list _RPAREN body
	 
	{
	 
		clear_symbols(fun_idx + 1);
	 
		var_num = 0;
		code("\n@%s_exit:", $2);
		code("\n\t\tMOV \t%%14,%%15");
		code("\n\t\tPOP \t%%14");
		code("\n\t\tRET");
	 
	}

  | _TYPE _LSQBRACKET _RSQBRACKET _ID
	{
	 
		fun_idx = lookup_symbol($4, FUN);
	 
		if(fun_idx == NO_INDEX){
	 
		int* param_types = (int*) malloc(sizeof(int)*128);
	 
		fun_idx = insert_symbol($4, FUN, $1, NO_ATR, 1);
	 
		parameter_map[fun_idx] = param_types;

		code("\n@%s_exit:", $4);
		code("\n\t\tMOV \t%%14,%%15");
		code("\n\t\tPOP \t%%14");
		code("\n\t\tRET");
	 
		}
	 
	else
	 
		err("redefinition of function '%s'", $4);

		code("\n%s:", $4);
		code("\n\t\tPUSH\t%%14");
		code("\n\t\tMOV \t%%15,%%14");
	 
	}
	 
	_LPAREN parameter_list _RPAREN body
	 
	{
	 
		clear_symbols(fun_idx + 1);
	 
		var_num = 0;

		code("\n@%s_exit:", $4);
		code("\n\t\tMOV \t%%14,%%15");
		code("\n\t\tPOP \t%%14");
		code("\n\t\tRET");
	 
	}
  ;

parameter_list
  : /* empty */
 
{ set_atr1(fun_idx, 0); }
  | parameters
  ;
parameters
  : parameter
  | parameters _COMMA parameter
  ;

parameter
  : parameter_value
  | parameter_reference
  ;

parameter_reference
  : parameter_value _AMPERSAND
  ;

parameter_value
  : _TYPE _ID
 
	{
	 
		if(lookup_symbol($2, PAR) != -1){
		 
		err("Redefinition of parameter %s ", $2);
	 
		}
	 
		insert_symbol($2, PAR, $1, 1, NO_ATR);
	 
		int num_params = get_atr1(fun_idx);
	 
		int* param_types = parameter_map[fun_idx];
		param_types[num_params] = $1;
		
		parameter_array_map[fun_idx][num_params] = 0;
		num_params += 1;
		set_atr1(fun_idx, num_params);
	}

  | _TYPE _LSQBRACKET _RSQBRACKET _ID
	{
		if(lookup_symbol($4, PAR) != -1){
		 
		err("Redefinition of parameter %s ", $4);
	 
		}
	 
		insert_symbol($4, PAR, $1, 1, 1);
	 
		int num_params = get_atr1(fun_idx);
	 
		int* param_types = parameter_map[fun_idx];
		param_types[num_params] = $1;
		parameter_array_map[fun_idx][num_params] = 1;
		num_params += 1;
		set_atr1(fun_idx, num_params);
	}
  ;

body
  : _LBRACKET variable_list 
	{
	  if(var_num)
          	code("\n\t\tSUBS\t%%15,$%d,%%15", 4*var_num);
          code("\n@%s_body:", get_name(fun_idx));
	}
	statement_list _RBRACKET
  ;

variable_list
  : /* empty */
  | variable_list variable
  ;


variable
  : _TYPE _ID _SEMICOLON
      {
        if(lookup_symbol($2, VAR|PAR) == NO_INDEX)
           insert_symbol($2, VAR, $1, ++var_num, NO_ATR);
        else 
           err("redefinition of '%s'", $2);
      }
  | _TYPE _LSQBRACKET _RSQBRACKET _ID _SEMICOLON
	{
		if(lookup_symbol($4, VAR|PAR) == NO_INDEX)
		   insert_symbol($4, VAR, $1, ++var_num, 1);
		else 
		   err("redefinition of '%s'", $4);
        }
  ;


statement_list
  : /* empty */
  | statement_list statement
  ;

statement
  : compound_statement
  | assignment_statement
  | if_statement
  | return_statement
  | inc_statement
  | dec_statement
  | for_statement
  | push_statement
  | break_statement
  | continue_statement
  ;

break_statement 
  : _BREAK _SEMICOLON
  ;

continue_statement
  : _CONTINUE _SEMICOLON
  ;

compound_statement
  : _LBRACKET statement_list _RBRACKET
  ;

assignment_statement
  : _ID _ASSIGN num_exp _SEMICOLON
      {
        int idx = lookup_symbol($1, VAR|PAR);
        if(idx == NO_INDEX)
          err("invalid lvalue '%s' in assignment", $1);
        else
	  if (get_atr2(idx)) err("Dodeljivanje vrednosti nije moguce nad nizovima, koristite '<<'");
          if(get_type(idx) != get_type($3))
            err("incompatible types in assignment");
	gen_mov($3, idx);
      }
  ;

push_statement
  : _ID _PUSH num_exp _SEMICOLON
    {
	int idx = lookup_symbol($1, VAR|PAR);
	int atr2 = get_atr2(idx);

	if (atr2 != 1)
	  err("Pushovanje elemenata je moguce samo nad nizovima");
	if(idx == NO_INDEX)
          err("invalid lvalue '%s' in assignment", $1);
        else
          if(get_type(idx) != get_type($3))
            err("incompatible types in assignment");
        
	int litinx = get_last_element();
	set_atr1(litinx, idx);


    }
  ;

num_exp
  : exp
  | num_exp _AROP exp
      {
        if(get_type($1) != get_type($3))
          err("invalid operands: arithmetic operation");
	int t1 = get_type($1);    
        code("\n\t\t%s\t", ar_instructions[$2 + (t1 - 1) * AROP_NUMBER]);
        gen_sym_name($1);
        code(",");
        gen_sym_name($3);
        code(",");
        free_if_reg($3);
        free_if_reg($1);
        $$ = take_reg();
        gen_sym_name($$);
        set_type($$, t1);
      }
  ;

exp
  : literal
  | _ID
      {
        $$ = lookup_symbol($1, VAR|PAR|GVAR);
        if($$ == NO_INDEX)
          err("'%s' undeclared", $1);
      }
  | function_call
  | _LPAREN num_exp _RPAREN
      { $$ = $2; }
  ;

literal
  : _INT_NUMBER
      { $$ = insert_literal($1, INT); }

  | _UINT_NUMBER
      { $$ = insert_literal($1, UINT); }
  ;

function_call
  : _ID
 
	{
	 
		fcall_idx = lookup_symbol($1, FUN);
		 
		if(fcall_idx == NO_INDEX)
		 
		err("'%s' is not a function", $1);
		 
		
		arg_counter = 0;
		array_param_counter = 0;
	 
	}
 
	_LPAREN argument_list _RPAREN
 
	{
 
		if(get_atr1(fcall_idx) != arg_counter)
		 
		err("wrong number of args to function '%s'",
		 	get_name(fcall_idx));
		
		code("\n\t\t\tCALL\t%s", get_name(fcall_idx));
		if(arg_counter > 0)
          		code("\n\t\t\tADDS\t%%15,$%d,%%15", arg_counter * 4);
		set_type(FUN_REG, get_type(fcall_idx));
		 
		$$ = FUN_REG;
		 
		arg_counter = 0;
		array_param_counter = 0;
 
	}
	
  ;

argument_list
  : /* empty */
  | arguments
  ;

arguments
  : argument
  | arguments _COMMA argument
  ;

argument
  : num_exp
 
	{
	 
		if(parameter_map[fcall_idx][arg_counter] != get_type($1) || parameter_array_map[fcall_idx][array_param_counter] != get_atr2($1))
		 
		err("incompatible type for argument in '%s'",
		 
		get_name($1));
	
		arg_counter += 1;
		array_param_counter += 1;
	 
	}
  ;

if_statement
  : if_part %prec ONLY_IF
  | if_part _ELSE statement
  ;

for_statement
  : for_part %prec ONLY_FOR
  | for_part _ELSE statement
  ;

for_part
  : _FOR _LPAREN _TYPE _ID
  {
 
	int i = lookup_symbol($4, PAR|VAR);
	 
	if(i != -1)
	 
		err("redefinition of variable '%s'", $4);
	 
	else
	 
		$<i>$ = insert_symbol($4, VAR, $3, 1, NO_ATR);
  }
  _ASSIGN literal
  {

  if($3 != get_type($7))
	 
  err("incompatible types in assignment");

  }

  _SEMICOLON rel_exp _SEMICOLON _ID
  {
  	$<i>$ = lookup_symbol($12, VAR);
  	if($<i>5 != $<i>$)
  	err("wrong var for increment");
  }
  inc_dec_token _RPAREN statement
  {
  	clear_symbols($<i>5);
  } 
  ;

if_part
  : _IF _LPAREN rel_exp _RPAREN statement
  ;

rel_exp
  : num_exp _RELOP num_exp
      {
        if(get_type($1) != get_type($3))
          err("invalid operands: relational operator");
      }
  ;

return_statement
  : _RETURN num_exp _SEMICOLON
      {
        if(get_type(fun_idx) != get_type($2) || get_atr2(fun_idx) != get_atr2($2))
          err("incompatible types in return");
	  gen_mov($2, FUN_REG);
          code("\n\t\tJMP \t@%s_exit", get_name(fun_idx));      
      }
  ;

inc_statement
  : _ID
	{
	  if(lookup_symbol($1, FUN) != NO_INDEX)
		{err("Postincrement may only be used on variables");
			}
		int indx = lookup_symbol($1, VAR|PAR);
		if (indx == NO_INDEX) {
			err("%s is not declared as variable", $1);
		}
	}
  _INC _SEMICOLON
  ;


dec_statement
  : _ID
	{
	  if(lookup_symbol($1, FUN) != NO_INDEX)
		{err("Postincrement may only be used on variables");
			}
		int indx = lookup_symbol($1, VAR|PAR);
		if (indx == NO_INDEX) {
			err("%s is not declared as variable", $1);
		}
	}
  _DEC _SEMICOLON
  ;

inc_dec_token
  : _INC | _DEC;

%%

int yyerror(char *s) {
  fprintf(stderr, "\nline %d: ERROR: %s", yylineno, s);
  error_count++;
  return 0;
}

void warning(char *s) {
  fprintf(stderr, "\nline %d: WARNING: %s", yylineno, s);
  warning_count++;
}

int main() {
  int synerr;
  init_symtab();
  output = fopen("output.asm", "w+");

  synerr = yyparse();
  print_symtab();
  clear_symtab();
  fclose(output);
  
  if(warning_count)
    printf("\n%d warning(s).\n", warning_count);

  if(error_count) {
    remove("output.asm");
    printf("\n%d error(s).\n", error_count);
  }

  if(synerr)
    return -1;  //syntax error
  else if(error_count)
    return error_count & 127; //semantic errors
  else if(warning_count)
    return (warning_count & 127) + 127; //warnings
  else
    return 0; //OK
}

